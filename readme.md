# Command tips 

## Terminal 
------------

Start new terminal on ubuntu:
```
Ctrl + Alt + t
```

Go to start of command
```
Ctrl + a
```

Go to end of command
```
Ctrl + e
```

Move by word through command
```
Ctrl + left arrow
Ctrl + right arrow
```

Delete by word through command
```
Ctrl + suppr        # Delete from start
Ctrl + backspace    # Delete from end
```

Search command from ~/.bash_history
```
Ctrl + r 
```

Get argument from last command
```
esc + . 
```

Back to the latest workspace
```
cd -
```


## Network 
----------

Capture packages 
```
tcpdump -i eth0 port 443            # From specific port 
tcpdump -i eth0 src 192.168.1.5     # From specific source 
tcpdump -i eth0 dst 172.217.19.142  # From specific destination
```